#include <DNSServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <FastLED.h>
#include <RunningMedian.h>
#include <WiFiClientSecureBearSSL.h>
#include "WiFiManager.h"
#include <WiFiUdp.h>
#include <Wire.h>

#define PWM_PIN 5
#define LEFT_STRIP_PIN 4
#define RIGHT_STRIP_PIN 0
#define NUM_LEDS 10
#define NUM_MODES 9

CRGB left_leds[NUM_LEDS];
CRGB right_leds[NUM_LEDS];

WiFiManager wifiManager;
IPAddress broadcastIP;
RunningMedian samples = RunningMedian(5);

int currentMode = 0;


void logUDP(String message, IPAddress ip) {
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }

    WiFiUDP Udp;

    // Listen with `nc -kul 37243`.
    Udp.beginPacket(ip, 37243);
    Udp.write(("(" PROJECT_NAME  ": " + String(millis()) + " - " + WiFi.localIP().toString() + ") " + ": " + message + "\n").c_str());
    Udp.endPacket();
}


void debug(const char* text) {
    logUDP(text, broadcastIP);
}


void doHTTPUpdate() {
    digitalWrite(LED_BUILTIN, LOW);
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }
    debug("[update] Looking for an update from v" VERSION ".");

    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
    client->setFingerprint(OTA_FINGERPRINT);
    t_httpUpdate_return ret = ESPhttpUpdate.update(*client, "https://" OTA_HOSTNAME "/" PROJECT_NAME "/", VERSION);
    switch (ret) {
    case HTTP_UPDATE_FAILED:
        debug("[update] Update failed.");
        break;
    case HTTP_UPDATE_NO_UPDATES:
        debug("[update] No update from v" VERSION ".");
        break;
    case HTTP_UPDATE_OK:
        debug("[update] Update ok.");
        break;
    }
    digitalWrite(LED_BUILTIN, HIGH);
    debug("Ready.");
}


void connectToWiFi() {
    debug("Connecting to WiFi...");
    WiFi.forceSleepWake();
    WiFi.hostname("LEDZeppelin");
    wifiManager.autoConnect("LEDZeppelin");
    broadcastIP = WiFi.localIP();
    broadcastIP[3] = 255;
    debug("Connected.");
}


void swirl(int palette, int speed) {
    int ledNo;
    char palettes[4][10][3] = {
        {
            // Colors.
            {3, 71, 50},
            {3, 71, 50},
            {0, 129, 72},
            {0, 129, 72},
            {198, 192, 19},
            {198, 192, 19},
            {239, 138, 23},
            {239, 138, 23},
            {239, 41, 23},
            {239, 41, 23},
        },
        {
            // White streaks.
            {0, 0, 0},
            {28, 28, 28},
            {56, 56, 56},
            {84, 84, 84},
            {112, 112, 112},
            {140, 140, 140},
            {168, 168, 168},
            {196, 196, 196},
            {224, 224, 224},
            {252, 252, 252},
        },
        {
            // White dot.
            {255, 255, 255},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
        },
        {
            // Purple to pink.
            {244, 50, 175},
            {231, 54, 180},
            {218, 59, 186},
            {206, 83, 191},
            {193, 68, 196},
            {180, 72, 202},
            {167, 77, 207},
            {155, 81, 212},
            {142, 85, 218},
            {129, 90, 223},
        },
    };
    ledNo = (millis() / speed) % NUM_LEDS;
    for (int i = 0; i < NUM_LEDS; i++) {
        left_leds[(ledNo + i) % NUM_LEDS] = right_leds[(ledNo + i) % NUM_LEDS] = CRGB(palettes[palette][i][0], palettes[palette][i][1], palettes[palette][i][2]);
    }
}


void fade(CRGB color, float speed) {
    float fadePercentage = abs((((unsigned long)(millis() * speed)) % 200) - 100) / 100.0;
    fill_solid(left_leds, NUM_LEDS, color.nscale8_video(fadePercentage * 255));
    fill_solid(right_leds, NUM_LEDS, color.nscale8_video(fadePercentage * 255));
}


void kitt(int speed) {
    int currentLED = abs(((NUM_LEDS * 2) - 1) - (int)(millis() / speed) % ((NUM_LEDS * 4) - 2));
    FastLED.clear();
    if (currentLED < NUM_LEDS) {
        right_leds[NUM_LEDS - 1 - currentLED] = CRGB::Red;
    } else {
        left_leds[abs(currentLED - NUM_LEDS)] = CRGB::Red;
    }
}

void showLEDMode() {
    switch (currentMode) {
    case 1:
        kitt(60);
        break;
    case 2:
        swirl(0, 50);
        break;
    case 3:
        swirl(1, 100);
        break;
    case 4:
        swirl(2, 50);
        break;
    case 5:
        fill_solid(left_leds, NUM_LEDS, CRGB::Red);
        fill_solid(right_leds, NUM_LEDS, CRGB::Green);
        break;
    case 6:
        fill_solid(left_leds, NUM_LEDS, CRGB::Blue);
        fill_solid(right_leds, NUM_LEDS, CRGB::Blue);
        break;
    case 7:
        fill_gradient_RGB(left_leds, NUM_LEDS, CRGB(244, 50, 175), CRGB(91, 103, 239));
        fill_gradient_RGB(right_leds, NUM_LEDS, CRGB(244, 50, 175), CRGB(91, 103, 239));
        break;
    case 8:
        swirl(3, 50);
        break;
    default:
        FastLED.clear();
        break;
    }
    FastLED.show();
}


// Get the latest PWM value, averaging it to remove outliers.
unsigned long getPWMValue() {
    unsigned long pwmValue = pulseIn(PWM_PIN, HIGH);
    samples.add(pwmValue);
    return samples.getMedian();
}

void startUpdateMode() {
    // Update mode.
    fill_solid(left_leds, NUM_LEDS, CRGB::Red);
    fill_solid(right_leds, NUM_LEDS, CRGB::Red);
    FastLED.show();
    delay(30);

    connectToWiFi();
    doHTTPUpdate();
}

void setup() {
    pinMode(PWM_PIN, INPUT);
    FastLED.addLeds<NEOPIXEL, LEFT_STRIP_PIN>(left_leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
    FastLED.addLeds<NEOPIXEL, RIGHT_STRIP_PIN>(right_leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
    FastLED.setMaxPowerInVoltsAndMilliamps(5, 1000);
    FastLED.clear();

    Serial.begin(115200);
    if (random(10) == 0) {
        startUpdateMode();
    }

    WiFi.forceSleepBegin(0);
}


void loop() {
    int pwmValue = getPWMValue();

    if (pwmValue < 980 || pwmValue > 2300) {
        // Error condition (also triggerable by switch).
        fade(CRGB::Red, 0.2);
        FastLED.show();
        delay(10);
        return;
    }

    if (pwmValue > 2100) { // This goes up to 2138.
        startUpdateMode();
    } else if (pwmValue >= 1954) {
        // Throttle fill mode.
        float percentage = constrain(pwmValue - 1954, 0, 50) / 50.0;

        FastLED.clear();
        fill_solid(left_leds, int(NUM_LEDS * percentage), CRGB::White);
        fill_solid(right_leds, int(NUM_LEDS * percentage), CRGB::White);
        FastLED.show();
    } else {
        currentMode = ((pwmValue - 988) / 1024.0) * NUM_MODES;
        showLEDMode();
    }

    FastLED.delay(10);
}
